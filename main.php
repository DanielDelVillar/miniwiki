<?php
require_once 'conf/autoload.php';
require_once 'vendor/autoload.php';

define("ACCESS_LEVEL_APP_NONE",  -100);
define("ACCESS_LEVEL_APP_USER",   100);
define("ACCESS_LEVEL_APP_ADMIN",  500);
define("WIKI_PAGE_VIEW_LOGIN",'login');
define("WIKI_PAGE_VIEW_ALL", 'all');
define("WIKI_PAGE_VIEW_PAGE", 'view');

/**
 *
 */
$router = new \wikiapp\utils\Router();
$router->addRoute('/wiki/all/', '\wikiapp\control\WikiController', 'listAll',  ACCESS_LEVEL_APP_NONE);
$router->addRoute('/wiki/view/', '\wikiapp\control\WikiController', 'viewPage', ACCESS_LEVEL_APP_NONE);
$router->addRoute('/admin/login/', '\wikiapp\control\WikiController', 'login', ACCESS_LEVEL_APP_NONE);
$router->addRoute('default',     '\wikiapp\control\WikiController', 'listAll',  ACCESS_LEVEL_APP_NONE);
$http_req = new wikiapp\utils\HttpRequest();
$router->dispatch($http_req);

/*
function loadUsers(){
    $users = wikiapp\model\User::findAll();
    $form = '<h1>Les utilisateurs: </h1>
    <form method="post" action="">
    <select name="cboUser">
    <option value="-1" selected>Tous</option>';
    foreach ($users as $val){
        $form.= "<option value='$val->id'>".$val->login."</option>";
    }
    $form.='</select>
    <button type="submit" name="loadPageBtn">OK</button>
    <button type="submit" name="addUserBtn">Add</button>
    </form>';
        return $form;
}

function loadPages(){
    $users = wikiapp\model\Page::findAll();
    $form = '<h1>Les Pages: </h1>
    <form method="post" action="">
    <select name="cboUser">
    <option value="-1" selected>Tous</option>';
    foreach ($users as $val){
        $form.= "<option value='$val->id'>".$val->login."</option>";
    }
    $form.='</select>
    <button type="submit" name="loadPageBtn">OK</button>
    <button type="submit" name="addUserBtn">Add</button>
    </form>';
    return $form;
}

function loadAddUserForm(){
    $form='<form method="post" action="">
                <div><label for="login">Login: </label><input type="text" name="login"/></div>
                <div><label for="pass">Password: </label><input type="password" name="pass"/></div>
                <div><select name="level">
					    <option value="-100">Aucun</option>
					    <option value="100">Normal</option>
					    <option value="500">Administrateur</option>
                    </select>
                </div>
                <div>
                <button type="submit" name="addUser">Save</button> 
                <button type="reset">Nettoyer</button> 
                </div>
            </form>';
    return $form;
}

function loadPage($pages){

    $html = '<h1>Les Pages: </h1><ul>';
    foreach ($pages as $val){
        $html.='<li>'.$val->title.'</li>';
    }
    $html.'</ul>';
    return $html;
}

function reloadPage(){
    header('Refresh: 0');
}

echo loadUsers();

if(isset($_POST['loadPageBtn'])){
    $pages = "";
    if($_POST['cboUser']==-1){
        $pages = \wikiapp\model\Page::findAll();
    }else{
        $u = new \wikiapp\model\User();
        $u->id = $_POST['cboUser'];
        $pages = $u->getPages();
    }
    echo loadPage($pages);
}else if(isset($_POST['addUserBtn'])){
    echo loadAddUserForm();
}else if(isset($_POST['addUser'])){
    $l = $_POST['login'];
    $p = $_POST['pass'];
    $lev = $_POST['level'];
    if(is_null($l)|| is_null($p)|| is_null($lev)){
        //error
    }else{
        $u = new \wikiapp\model\User();
        $u->login = $l;
        $u->pass = $p;
        $u->level = $lev;
        print_r($u);
        $success = $u->save();
        echo "<p>$success</p>";
        if($success){
            echo "Bravo";
        }else{
            echo ":(";
        }
    }


}*/
