<?php
namespace wikiapp\utils;

require_once 'AbstractAuthentification.php';

use wikiapp\model\User;

class Authentification extends AbstractAuthentification
{

    public function __construct(){

    }


    public function login($login, $pass)
    {
        $users = User::findAll();
        $log=null;
        foreach ($users as $user) {
            if ($user->login == $login) {
                $log = $login;
                echo $log;
                return;
            }
        }
    }

    public function logout()
    {

    }

    public function checkAccessRight($requested)
    {
        return ($this->access_level >= $requested);
    }

    function createUser($user, $pass, $accesLevel){
            $phash = password_hash($pass, PASSWORD_DEFAULT);
            addUser($user, $phash, $accesLevel);
    }
}