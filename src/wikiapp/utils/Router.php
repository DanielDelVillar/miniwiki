<?php

namespace wikiapp\utils;


class Router extends AbstractRouter
{

    public function addRoute($url, $ctrl, $mth, $level)
    {
        // TODO: Implement addRoute() method.

        self::$routes[$url] = [$ctrl, $mth, $level];
    }
    public function dispatch(HttpRequest $http_request)

    {
        $controller =""; $action="";
        if(!is_null($http_request->path_info) && isset(self::$routes[$http_request->path_info])){
                $controller = self::$routes[$http_request->path_info][0];
                $action = self::$routes[$http_request->path_info][1];

        }else{
            $controller = self::$routes['default'][0];
            $action = self::$routes['default'][1];
        }
         $ctrl = new $controller($http_request);
            $ctrl->$action();
    }
}