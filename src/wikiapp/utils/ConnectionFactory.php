<?php
namespace wikiapp\utils;

class ConnectionFactory{
    static private  $config, $db;

    public static function setConfig($nom_fichier){
        self::$config = parse_ini_file($nom_fichier);
    }

    public static function makeConnection(){
        //utiliser la config stockée
        //construire le DSN et cree instance du PDO
        // qui sera stockée dans l'attribut statique.
        //Si une connexion existe déjà (i.e. l'attribut $db contient une valeur),
        // la méthode la retourne sans créer de nouvelle
        if(is_null(self::$db)){
            self::setConfig('conf/config.ini');
            $dsn = "mysql:host=".self::$config['host'].";dbname=".self::$config['dbname'].";charset=".self::$config['charset'];
            try {
                self::$db = new \PDO($dsn, self::$config['user'], self::$config['pass']);
            } catch(\PDOException $e) {
                /* Erreur de connexion */
                echo "Connection error: $dsn" . $e->getMessage();
                exit;
            }
        }
        return self::$db;
    }
}
