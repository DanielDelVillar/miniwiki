<?php

namespace wikiapp\control;

use wikiapp\utils\Authentification;
use wikiapp\model\Page;
use wikiapp\utils\HttpRequest;
use wikiapp\view\AbstractView;
use wikiapp\view\WikiView;

class WikiController {

    /* Attribut pour stocker l'objet HttpRequest */ 
    private $request=null;
    
    public function __construct(HttpRequest $http_req){
        $this->request = $http_req ;
    }


    public function listAll(){
        
        /*
         * Algorithme :  
         * 
         * - Récupérer une liste de toutes les pages (Page::findAll)
         * - Afficher une liste des titres des article 
         *  
         */
       $pages = Page::findAll();
        $wv = new WikiView($pages);
        $wv->render(WIKI_PAGE_VIEW_ALL);
    }
    
    /* 
     * Méthode viewPage
     *  
     * Réalise la fonctionnalité : "afficher un article" 
     *
     */ 

    public function viewPage(){
        
        /*
         * Algorithme :  
         * 
         * - Le titre de la page est dans le paramètre de l'URL ($_GET)
         * - Récupérer la page en question (Page::findByTitle)
         * - Afficher le contenu de l'article
         * - Afficher la date de modification de la page et le nom de l'auteur 
         * 
         */
        $page = Page::findByTitle($_GET['title']);
        $wv = new WikiView($page);
        $wv->render(WIKI_PAGE_VIEW_PAGE);

    }

    public function login()
    {
        $login=null;
        $pass=null;
        Authentification::login($login,$pass);
    }

}