<?php
/**
 * Created by PhpStorm.
 * User: dano
 * Date: 2/11/16
 * Time: 11:52 AM
 */

namespace wikiapp\control;

class WikiAdminController {
    private $request=null;

    public function __construct(\wikiapp\utils\HttpRequest $http_req){
        $this->request = $http_req ;
    }



    /*
     * MÃ©thode loginUser
     *
     * Affiche le formulaire d'authentification
     *
     */

    public function loginUser() {

        /*
         * Algorithme :
         *
         *  - CrÃ©er une instance de la classe WikiAdminView
         *  - execute la vue qui affiche le formulaire de connexion.
         *
         */



    }

    /*
     * MÃ©thode checkUser
     *
     * Verifie l'identifiant et le mot de passe fournie par l'utilisateur
     *
     */

    public function checkUser(){


        /*
         *  Algorithme :
         *
         * - RÃ©cupÃ©rer les donnÃ©es du formulaire de connexion
         * - Filtrer les donnÃ©e !!!
         * - CrÃ©er une instance de la classe Authentification et verifier
         *   l'identifiant et le mots de passe

         *   => (Indication : pour simplifier on peut coder une mÃ©thode
         *       User::findByName() qui retourne un objet User pour avoir
         *       les information den BD)

         * - Si les informations sont correctes
         *   - afficher l'espace personel de l'utilisateur
         * - sinon
         *   - afficher le formulaire de connexion.
         *
         */

    }

    /*
     * MÃ©thode logoutUser
     *
     * RÃ©alise la deconnexion d'un utilisateur
     *
     */

    public function logoutUser(){

        /*
         * Algorithme :
         *
         * - ExÃ©cute la mÃ©thode de decconexion de la classe Authentification
         * - ExÃ©cute la fonctionalitÃ© par dÃ©faut de l'application
         *
         */


    }


    /*
     * userSpace
     *
     * RÃ©alise la fonctionnalitÃ© "afficher l'espace de l'utilisateur"
     *
     */

    public function userSpace(){


        /*
         * Algorithme :
         *
         * - si l'utilisateur est connectÃ©
         *    - rÃ©cupÃ©rer les articles de l'utilisteur
         *    - creer la vue necÃ©ssaire et afficher les article
         * - sinon
         *    - afficher le formulaire de connexion.
         */

    }

}