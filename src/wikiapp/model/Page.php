<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 10/10/16
 * Time: 14:34
 */

namespace wikiapp\model;


use wikiapp\utils\ConnectionFactory;
use wikiapp\utils\Constants;

class Page extends AbstractModel
{

    private $id, $title, $text, $date, $author;

    public function __construct()
    {
        $this->db = ConnectionFactory::makeConnection();
    }

    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name))
            return $this->$attr_name;
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name))
            $this->$attr_name=$attr_val;
        else{
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    protected function update()
    {
        $update = 'UPDATE '.Constants::PAGE_TABLE.' SET title = :title, text = :text, date = :date, author = :author where id = :id';
        $update_prep = $this->db->prepare($update);
        $update_prep->bindParam(':title', $this->title, \PDO::PARAM_STR);
        $update_prep->bindParam(':text', $this->text, \PDO::PARAM_STR);
        $update_prep->bindParam(':date', $this->date, \PDO::PARAM_STR);
        $update_prep->bindParam(':author', $this->author, \PDO::PARAM_INT);
        $update_prep->bindParam(':id', $this->id, \PDO::PARAM_INT);
        $nb_lines = $this->db->exec($update_prep);
        return $nb_lines;
    }

    protected function insert()
    {
        $insert = 'INSERT INTO '.Constants::PAGE_TABLE.' values(null, :title, :text, :date, :author)';
        $insert_prep = $this->db->prepare($insert);
        $insert_prep->bindParam(':title', $this->title, \PDO::PARAM_STR);
        $insert_prep->bindParam(':text', $this->text, \PDO::PARAM_STR);
        $insert_prep->bindParam(':date', $this->date, \PDO::PARAM_STR);
        $insert_prep->bindParam(':author', $this->author, \PDO::PARAM_INT);
        $nb_lines = $this->db->exec($insert_prep);
        return $nb_lines;
    }

    public function save()
    {
        if(is_null($this->id)){
            return ($this->insert()>-1);
        }else{
            return $this->update();
        }
    }

    public function delete()
    {
        $delete = 'DELETE FROM '. Constants::PAGE_TABLE. ' where id = :id';
        $delete_prep = $this->db->prepare($delete);
        $delete_prep->bindParam(':id', $this->id, \PDO::PARAM_INT);
        $nb_lines = $this->db->exec($delete_prep);
        return $nb_lines;
    }

    static public function findById($id)
    {
        $db = ConnectionFactory::makeConnection();
        $selectById = 'SELECT * FROM ' . Constants::PAGE_TABLE . ' where id = :id';
        $selectById_prep = $db->prepare($selectById);
        $selectById_prep->bindParam(':id', $id, \PDO::PARAM_INT);
        if ($selectById_prep->execute()) {
            return $selectById_prep->fetchObject(__CLASS__);
        }else{
            return null;
        }
    }

    static public function findByTitle($title)
    {
        $db = ConnectionFactory::makeConnection();
        $selectById = 'SELECT * FROM ' . Constants::PAGE_TABLE . ' where title = :title';
        $selectById_prep = $db->prepare($selectById);
        $selectById_prep->bindParam(':title', $title, \PDO::PARAM_INT);
        if ($selectById_prep->execute()) {
            return $selectById_prep->fetchObject(__CLASS__);
        }else{
            return null;
        }
    }

    static public function findAll()
    {
        $db = ConnectionFactory::makeConnection();
        $select = 'SELECT * FROM '. Constants::PAGE_TABLE;
        $resultat = $db->query($select);
        if ($resultat) {
            return $resultat->fetchAll(\PDO::FETCH_CLASS, __CLASS__);
        }else{
            return null;
        }
    }

    public function getAuthor(){
        $select = 'SELECT * FROM '. Constants::USER_TABLE.' where id = :id';
        $select_prep = $this->db->prepare($select);
        $select_prep->bindParam(":id", $this->author);
        if($select_prep->execute()){
            return $select_prep->fetchObject(User::class);
        }else{
            return null;
        }
    }
}