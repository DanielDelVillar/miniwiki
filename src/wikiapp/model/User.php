<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 10/10/16
 * Time: 14:32
 */
namespace wikiapp\model;

use wikiapp\utils\ConnectionFactory;
use wikiapp\utils\Constants;

class User extends AbstractModel
{
    private $id, $login, $pass, $level;

    public function __construct()
    {
        $this->db = ConnectionFactory::makeConnection();
    }

    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name))
            return $this->$attr_name;
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name))
            $this->$attr_name=$attr_val;
        else{
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    protected function update()
    {
        $update = 'UPDATE '.Constants::USER_TABLE.' SET login = :login, pass = :pass, level = :level where id = :id';
        $update_prep = $this->db->prepare($update);
        $update_prep->bindParam(':login', $this->login, \PDO::PARAM_STR);
        $update_prep->bindParam(':pass', $this->pass, \PDO::PARAM_STR);
        $update_prep->bindParam(':level', $this->level, \PDO::PARAM_INT);
        $update_prep->bindParam(':id', $this->id, \PDO::PARAM_INT);
        $nb_lines = $this->db->exec($update_prep);
        return $nb_lines;
    }

    protected function insert()
    {
        $insert = 'INSERT INTO '.Constants::USER_TABLE.' values(null, :login, :pass, :level)';
        $insert_prep = $this->db->prepare($insert);
        $p_hash = password_hash($this->pass, PASSWORD_DEFAULT);
        $insert_prep->bindParam(':login', $this->login, \PDO::PARAM_STR);
        $insert_prep->bindParam(':pass', $p_hash, \PDO::PARAM_STR);
        $insert_prep->bindParam(':level', $this->level, \PDO::PARAM_INT);
        $nb_lines = $insert_prep->execute();
        return $nb_lines;
    }

    public function save()
    {
        if(is_null($this->id)){
            return ($this->insert()>-1);
        }else{
            return $this->update();
        }

    }

    public function delete()
    {
        $delete = 'DELETE FROM '. Constants::USER_TABLE. ' where id = :id';
        $delete_prep = $this->db->prepare($delete);
        $delete_prep->bindParam(':id', $this->id, \PDO::PARAM_INT);
        $nb_lines = $this->db->exec($delete_prep);
        return $nb_lines;
    }

    static public function findById($id)
    {
        $db = ConnectionFactory::makeConnection();
        $selectById = 'SELECT * FROM ' . Constants::USER_TABLE . ' where id = :id';
        $selectById_prep = $db->prepare($selectById);
        $selectById_prep->bindParam(':id', $id, PDO::PARAM_INT);
        if ($selectById_prep->execute()) {
            return $selectById_prep->fetchObject(__CLASS__);
        }else{
            return null;
        }
    }

    static public function findAll()
    {
        $db = ConnectionFactory::makeConnection();
        $select = 'SELECT * FROM '.Constants::USER_TABLE;
        $resultat = $db->query($select);
        if ($resultat) {
            return $resultat->fetchAll(\PDO::FETCH_CLASS, __CLASS__);
        }else{
            return null;
        }
    }

    public function getPages(){

        $select = 'SELECT * FROM '. Constants::PAGE_TABLE.' where author = :id';
        $select_prep = $this->db->prepare($select);
        $select_prep->bindParam(":id", $this->id);
        if($select_prep->execute()){
            return $select_prep->fetchAll(\PDO::FETCH_CLASS, Page::class);
        }else{
            return null;
        }

    }
}