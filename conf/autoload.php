<?php
spl_autoload_register('my_autoload');
function my_autoload($classNameWithNameSpace){
    try {
        $classNameWithNameSpace = "src/" . $classNameWithNameSpace . ".php";
        require_once str_replace('\\', '/', $classNameWithNameSpace);
    }catch (\Exception $e){
        echo $e->getMessage();
        echo $e->getTrace();
    }
}
